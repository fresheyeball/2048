{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms   #-}


module Main where


import           Control.Monad.IO.Class       (MonadIO (liftIO))
import           Data.Map                     as M (Map, elems, fromList,
                                                    insertWith, keys, lookup,
                                                    mapKeys, mapMaybe,
                                                    singleton, toList)
import           Data.Set                     as Set (fromList, toList)
import           Data.Text                    (Text, pack, toLower)
import           System.Random                (Random (randomRIO))

import           Shpadoinkle                  (Html, JSM, MonadJSM (..))
import           Shpadoinkle.Backend.Snabbdom (runSnabbdom)
import           Shpadoinkle.Html             as H (ClassListRep (asClass),
                                                    addStyle, body, body_,
                                                    class', div, getBody, h1,
                                                    onKeyupM, text,
                                                    textProperty)
import           Shpadoinkle.Keyboard         (pattern DownArrow, KeyCode,
                                               pattern LeftArrow,
                                               pattern RightArrow,
                                               pattern UpArrow)
import           Shpadoinkle.Run              (liveWithStatic, runJSorWarp,
                                               simple)


data Row = R1 | R2 | R3 | R4
  deriving (Eq, Ord, Show, Enum, Bounded)


data Column = C1 | C2 | C3 | C4
  deriving (Eq, Ord, Show, Enum, Bounded)


data Tile = T2 | T4 | T8 | T16 | T32 | T64 | T128 | T256 | T512 | T1024 | T2048
  deriving (Eq, Ord, Show, Enum, Bounded)


type Board = Map Row (Map Column Tile)


human :: Tile -> Text
human = \case
  T2     ->    "2"
  T4     ->    "4"
  T8     ->    "8"
  T16    ->   "16"
  T32    ->   "32"
  T64    ->   "64"
  T128   ->  "128"
  T256   ->  "256"
  T512   ->  "512"
  T1024  -> "1024"
  T2048  -> "2048"


merge :: Board -> Board
merge old = M.fromList . zip [minBound..maxBound] . combine . elems <$> old
  where
  combine (t:t':ts) | t == t' = succ t : combine ts
  combine (t:ts)    = t : combine ts
  combine []        = []


board :: Board -> Html m a
board state = H.div "stage" $  do
  row    <- [minBound..maxBound]
  column <- [minBound..maxBound]
  return . tile row column $ M.lookup row state >>= M.lookup column


getEmpties :: Board -> [(Row,Column)]
getEmpties b = blankRows <> nonBlankRows
  where
  blankRows = do
    r <- Prelude.filter (not . flip elem (M.keys b)) [minBound..maxBound]
    c <- [minBound..maxBound]
    return (r,c)

  nonBlankRows = do
    (r, cs) <- M.toList b
    k <- Prelude.filter (not . flip elem (M.keys cs)) [minBound..maxBound]
    return (r,k)


stylePosition :: Row -> Column -> Text
stylePosition r c = "transform:translate3d(" <> px c <> ", " <> px r <> ", 0);"
  where px x = pack (show (fromEnum x * 120)) <> "px"


tile :: Row -> Column -> Maybe Tile -> Html m a
tile r c t = H.div
  [ class' [ "tile", asClass . toLower $ maybe "nothing" (pack . show) t ]
  , textProperty "style" $ stylePosition r c
  ]
  [ text $ maybe "" human t
  ]


flipBoard :: Board -> Board
flipBoard = fmap $ \cs -> M.fromList $ do
  (c, t) <- M.toList cs
  return (case c of
    C1 -> C4; C2 -> C3; C3 -> C2; C4 -> C1, t)


transpose :: Board -> Board
transpose m =
  M.fromList . fmap (\r -> (cr r, M.mapKeys rc $ M.mapMaybe (M.lookup r) m))
  . Set.toList . Set.fromList . concat . M.elems $ M.keys <$> m
  where rc = \case R1 -> C1; R2 -> C2; R3 -> C3; R4 -> C4;
        cr = \case C1 -> R1; C2 -> R2; C3 -> R3; C4 -> R4;


addRandom2 :: MonadIO m => Board -> m Board
addRandom2 b = liftIO $ do
  let es = getEmpties b
  i <- randomRIO (0, length es - 1)
  let (r, c) = es !! i
  return $ insertWith (<>) r (M.singleton c T2) b


data Result = Win | Lose | Continue


checkResult :: Board -> Result
checkResult b
  | length allTiles == 4 * 4 = Lose
  | T2048 `elem` allTiles    = Win
  | otherwise                = Continue
  where allTiles = elems b >>= elems


data Model = Model KeyCode Board deriving Eq


view :: MonadJSM m => Model -> Html m Model
view (Model k' b) = case checkResult b of
  Lose     -> H.body_ [ H.h1 "lose" [ "You Lose :(" ]
                      , board b
                      ]
  Win      -> H.body_ [ H.h1 "win"  [ "You Win!" ]
                      , board b]
  Continue -> H.body  [ onKeyupM $ \k -> fmap const $
    let addK = fmap (Model k) . if k == k' then return else addRandom2
    in case k of
    LeftArrow  -> addK $ merge b
    RightArrow -> addK . flipBoard . merge $ flipBoard b
    UpArrow    -> addK . transpose . merge $ transpose b
    DownArrow  -> addK . transpose . flipBoard . merge . flipBoard $ transpose b
    _          -> return $ Model k' b
    ] [ board b ]


app :: JSM ()
app = do
  addStyle "style.css"
  initial <- addRandom2 mempty
  simple runSnabbdom (Model 0 initial) view getBody


dev :: IO ()
dev = liveWithStatic 8080 app "./static"


main :: IO ()
main = do
  putStrLn "\nHappy point of view on https://localhost:8080\n"
  runJSorWarp 8080 app
