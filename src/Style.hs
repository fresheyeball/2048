{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}


module Style where


import           Shpadoinkle.Html.TH.CSS


$(extractNamespace "./static/style.css")
